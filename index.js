const express = require('express');

const app = express();
const port = 8000;

const product = [
  {
    id: 1,
    merek: "samsung",
    type: "S20",
    price: 10000000
  },
  {
    id: 2,
    merek: "xiaomi",
    type: "poco",
    price: 1000000
  },
  {
    id: 3,
    merek: "apple",
    type: "x",
    price: 15000000
  },
  {
    id: 4,
    merek: "samsung",
    type: "s5",
    price: 8000000
  },
  {
    id: 5,
    merek: "xiaomi",
    type: "A5",
    price: 800000
  },
]

app.get('/product', (req, res) => {
  res.json(product)
})

app.get('/product/:merek', (req, res) => {
  const merek = req.params.merek;
  const productDetail = product.filter((item) => {
    return item.merek === merek
  });
  res.json(productDetail);
})

app.listen(port, () => console.log(`App sudah berjalan di port ${port}`));